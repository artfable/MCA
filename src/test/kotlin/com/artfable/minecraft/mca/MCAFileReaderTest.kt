package com.artfable.minecraft.mca

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.*
import kotlin.test.BeforeTest
import kotlin.test.Test

/**
 * @author artfable
 * @since 21/06/2022
 */
@ExtendWith(MockitoExtension::class)
internal class MCAFileReaderTest {
    private val random = Random()

    @Mock
    private lateinit var inputStream: InputStream

    @Mock
    private lateinit var header: MCAFileHeader

    private lateinit var mcaFileReader: MCAFileReader

    @BeforeTest
    fun setUp() {
        mcaFileReader = MCAFileReader(inputStream)
    }

    @Test
    fun readHeader() {
        val expected = Array(1024) { 0u }

        expected[0] = UInt.MAX_VALUE
        for (i in 1 until 1024) {
            expected[i] = random.nextInt().toUInt()
        }

        val expectedBytes = expected.asSequence()
            .flatMap { sequenceOf((it shr 24).toByte(), (it shr 16).toByte(), (it shr 8).toByte(), it.toByte()) }
            .toList()
            .toByteArray()

//        given
        `when`(inputStream.readNBytes(eq(4096))).thenReturn(expectedBytes)

        val header = mcaFileReader.readHeader()

        assertArrayEquals(expected, header.timestamps)
        header.locations.forEachIndexed { i, location ->
            assertEquals(expected[i] shr 8, location.first)
            assertEquals(expected[i].toUByte(), location.second)
        }

        verify(inputStream, times(2)).readNBytes(anyInt())
    }

    @Test
    fun readChunks() {
        val emptyPair: Pair<UInt, UByte> = Pair(0u, 0u)
        val locations = Array(1024) { emptyPair }

        val chunkSize = 2666
        val bigChunkSize = 6666

        val filledPart = ByteArray(4091) { -1 }
        val filledFull = filledPart + ByteArray(5) { -1 }
        val empty = ByteArray(4096) { 0 }

        locations[1] = Pair(3u, 2u)
        locations[4] = Pair(2u, 1u)
        locations[5] = Pair(5u, 2u)
        locations[8] = Pair(8u, 1u)

        ByteArrayOutputStream().use { out ->
            // 2 offset
            out.writeBytes(byteArrayOf(0, 0, (chunkSize shr 8).toByte(), chunkSize.toByte(), 2))
            out.writeBytes(filledPart)
            // 3
            out.writeBytes(byteArrayOf(0, 0, (bigChunkSize shr 8).toByte(), bigChunkSize.toByte(), 2))
            out.writeBytes(filledPart)
            out.writeBytes(filledFull)
            // 5
            out.writeBytes(byteArrayOf(0, 0, (chunkSize shr 8).toByte(), chunkSize.toByte(), 2))
            out.writeBytes(filledPart)
            out.writeBytes(filledFull)
            // gap
            out.writeBytes(empty)
            // 8
            out.writeBytes(byteArrayOf(0, 0, (chunkSize shr 8).toByte(), chunkSize.toByte(), 1))
            out.writeBytes(filledPart)

            mcaFileReader = MCAFileReader(ByteArrayInputStream(out.toByteArray()))
        }

        `when`(header.locations).thenReturn(locations)

        mockConstruction(Chunk::class.java) { mock, context ->
            `when`(mock.compressionType).thenReturn((context.arguments()[0] as Byte).toUByte())
        }.use {
            val chunks = mcaFileReader.readChunks(header)

            assertEquals(1024, chunks.size)
            // 3 offset
//            assertEquals(bigChunkSize, chunks[1]?.size)
            assertEquals(2u.toUByte(), chunks[1]?.compressionType)
            // 2
//            assertEquals(chunkSize, chunks[4]?.size)
            assertEquals(2u.toUByte(), chunks[4]?.compressionType)
            // 5
//            assertEquals(chunkSize, chunks[5]?.size)
            assertEquals(2u.toUByte(), chunks[5]?.compressionType)
            // 8
//            assertEquals(chunkSize, chunks[8]?.size)
            assertEquals(1u.toUByte(), chunks[8]?.compressionType)
        }
    }

}