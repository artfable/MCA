package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.*
import com.artfable.minecraft.mca.tags.Tag.TagId
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.mockStatic
import org.mockito.internal.matchers.Any
import org.mockito.internal.progress.ThreadSafeMockingProgress.mockingProgress
import org.mockito.junit.jupiter.MockitoExtension
import kotlin.reflect.jvm.javaMethod
import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * @author artfable
 * @since 23/06/2022
 */
@ExtendWith(MockitoExtension::class)
internal class ChunkTest {

    @Test
    fun decompress() {
        val compoundTag = CompoundTag("Test", listOf())

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(compoundTag)

            assertEquals(compoundTag, Chunk(2u, byteArrayOf()).decompressedData)
        }
    }

    @Test
    fun getBlockStateAt() {
        val block = CompoundTag("", listOf())
        val data = LongArray(256)

        data[0] = 0b10000

        val chunkData = CompoundTag("", listOf(
            StringTag("Status", "minecraft:full"),
            IntTag("xPos", 0),
            IntTag("zPos", 0),
            ListTag("sections", tagsId = TagId.COMPOUND, tags = listOf(
                CompoundTag("", listOf(
                    ByteTag("Y", 0),
                    CompoundTag("block_states", listOf(
                        ListTag("palette", tagsId = TagId.COMPOUND, tags = listOf(CompoundTag("wrong", listOf()), block.toCompoundTag())),
                        LongArrayTag("data", data)
                    ))
                ))
            ))
        ))

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(chunkData)

            assertEquals(block, Chunk(2u, byteArrayOf()).getBlockStateAt(1, 0,0))
        }
    }

    @Test
    fun getBlockStateAt_corruptedData() {
        val block = CompoundTag("", listOf())

        val chunkData = CompoundTag("", listOf(
            StringTag("Status", "minecraft:full"),
            IntTag("xPos", 0),
            IntTag("zPos", 0),
            ListTag("sections", tagsId = TagId.COMPOUND, tags = listOf(
                CompoundTag("", listOf(
                    ByteTag("Y", 0),
                    CompoundTag("block_states", listOf(
                        ListTag("palette", tagsId = TagId.COMPOUND, tags = listOf(CompoundTag("wrong", listOf()), block)),
                        LongArrayTag("data", longArrayOf(0b10000))
                    ))
                ))
            ))
        ))

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(chunkData)

            assertThrows<IllegalStateException> { Chunk(2u, byteArrayOf()).getBlockStateAt(0, 0,0) }
        }
    }

    @Test
    fun getBlockStateAt_1element() {
        val block = CompoundTag("block", listOf())

        val chunkData = CompoundTag("", listOf(
            StringTag("Status", "minecraft:full"),
            IntTag("xPos", 0),
            IntTag("zPos", 0),
            ListTag("sections", tagsId = TagId.COMPOUND, tags = listOf(
                CompoundTag("", listOf(
                    ByteTag("Y", 0),
                    CompoundTag("block_states", listOf(
                        ListTag("palette", tagsId = TagId.COMPOUND, tags = listOf(block.toCompoundTag()))
                    ))
                ))
            ))
        ))

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(chunkData)

            assertEquals(block, Chunk(2u, byteArrayOf()).getBlockStateAt(0, 0,0))
        }
    }

    @Test
    fun getBlockStateAt_invalidStatus() {
        val chunkData = CompoundTag("", listOf(StringTag("Status", "empty")))

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(chunkData)

            assertThrows<IllegalStateException> { Chunk(2u, byteArrayOf()).getBlockStateAt(0, 0,0) }
        }
    }

    @Test
    fun getBlockStateAt_outsideChunk() {
        val chunkData = CompoundTag("", listOf(
            StringTag("Status", "minecraft:full"),
            IntTag("xPos", 4),
            IntTag("zPos", 2)
        ))

        mockStatic(::readTag.javaMethod!!.declaringClass).use { tagReader ->
            tagReader.`when`<AbstractTag> { readTag(any(byteArrayOf().inputStream())) }.thenReturn(chunkData)

            assertThrows<IllegalArgumentException> { Chunk(2u, byteArrayOf()).getBlockStateAt(0, 0,0) }
        }
    }

    private fun <T> any(stub: T): T {
        mockingProgress().argumentMatcherStorage.reportMatcher(Any.ANY)
        return stub
    }
}