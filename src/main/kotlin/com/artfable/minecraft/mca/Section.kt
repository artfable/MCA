package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.*
import com.artfable.minecraft.mca.tags.Tag.TagId
import java.util.logging.Logger
import kotlin.math.ceil
import kotlin.math.log2
import kotlin.math.pow

/**
 * Represent section (16 * 16 * 16 cube of blocks) in a [Chunk]. [index] - is a 'Y' of that Section in the [Chunk]
 *
 * @author artfable
 * @since 10/07/2022
 */
data class Section(val index: Byte) {
    private companion object {
        private val log: Logger = Logger.getLogger(Section::class.simpleName)
    }

    private val palette: MutableList<Pair<ImmutableCompoundTag, Int>> = mutableListOf()
    private val data: IntArray = IntArray(4096) { 0 }

    constructor(index: Byte, startPalette: ListTag, startData: LongArrayTag?): this(index) {
        check(startPalette.tagsId == TagId.COMPOUND)

        startPalette.tags.forEach { block -> palette.add((block as CompoundTag) to 0) }

        if (palette.size > 1) startData?.let { initData(palette.size, it) }
    }

    private fun initData(paletteSize: Int, startData: LongArrayTag) {
        val bitsPerIndex = ceil(log2(paletteSize.toDouble())).toInt().let { if (it >= 4) it else 4 }
        val indexesPerLong = 64 / bitsPerIndex
        check(ceil(4096.0 / indexesPerLong).toInt() == startData.array.size) {
            "Wrong data size ${startData.array.size}, should be: ${ceil(4096.0 / indexesPerLong).toInt()}, palette: $paletteSize, bitPerIndex: $bitsPerIndex"
        }

        val mask = 2.0.pow(bitsPerIndex).toLong() - 1
        for (i in 0 until startData.array.size) {
            var longIndexes = startData.array[i]
            for (j in 0 until indexesPerLong) {
                val dataIndex = i * indexesPerLong + j
                if (dataIndex >= 4096) break

                val indexInPalette = (mask and longIndexes).toInt()
                data[dataIndex] = indexInPalette
                longIndexes = longIndexes shr bitsPerIndex
                palette[indexInPalette] = palette[indexInPalette].incremented()
            }
        }
    }

    fun getBlockStateAt(blockIndex: Int): ImmutableCompoundTag {
        return palette[data[blockIndex]].first
    }

    fun setBlockStateAt(blockIndex: Int, block: ImmutableCompoundTag) {
        palette[data[blockIndex]] = palette[data[blockIndex]].decremented()

        palette.findIndex(block)?.let { paletteIndex ->
            data[blockIndex] = paletteIndex
            palette[paletteIndex] = palette[paletteIndex].incremented()
        } ?: run {
            palette.add(block.copy() to 1)
            data[blockIndex] = palette.size - 1
        }
    }

    fun toRawData(): Pair<ListTag, LongArrayTag> {
        // TODO: optimise
        val rawPalette = palette.asSequence()
            .map(Pair<ImmutableCompoundTag, Int>::first)
            .map(ImmutableCompoundTag::toCompoundTag)
            .toList()


        val rawData = LongArrayTag("data", getCompressedData(rawPalette).toLongArray())

        return Pair(ListTag("palette", rawPalette, TagId.COMPOUND), rawData)
    }

    private fun getCompressedData(rawPalette: List<ImmutableCompoundTag>): List<Long> {
        val compressedData = mutableListOf<Long>()

        val bitsPerIndex = ceil(log2(rawPalette.size.toDouble())).toInt().let { if (it >= 4) it else 4 }
        val indexesPerLong = 64 / bitsPerIndex

        var accumulator = 0L
        var currentInLong = 0
        data.forEach { index ->
            accumulator += index.toLong() shl (currentInLong * bitsPerIndex)
            currentInLong++

            if (currentInLong == indexesPerLong) {
                compressedData.add(accumulator)
                accumulator = 0
                currentInLong = 0
            }
        }

        if (ceil(4096.0 / indexesPerLong).toInt() == compressedData.size + 1) {
            compressedData.add(accumulator)
        } else {
            if (accumulator != 0L) {
                log.severe("accumulator isn't empty ($accumulator), but no more indexes allowed")
            }
        }
        // TODO: test

        return compressedData
    }

    private fun MutableList<Pair<ImmutableCompoundTag, Int>>.contains(toFind: ImmutableCompoundTag): Boolean {
        return any { (block, _) ->
            block == toFind
        }
    }

    private fun MutableList<Pair<ImmutableCompoundTag, Int>>.findIndex(toFind: ImmutableCompoundTag): Int? {
        return indices.find { index ->
            this[index].first == toFind
        }
    }

    private fun Pair<ImmutableCompoundTag, Int>.incremented(): Pair<ImmutableCompoundTag, Int> {
        return copy(second = second + 1)
    }

    private fun Pair<ImmutableCompoundTag, Int>.decremented(): Pair<ImmutableCompoundTag, Int> {
        return copy(second = second - 1)
    }
}