package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.CompoundTag
import com.artfable.minecraft.mca.tags.ImmutableCompoundTag
import java.io.File
import java.io.OutputStream

/**
 * @author artfable
 * @since 14/06/2022
 */
class MCAFile(file: File) {
    val header: MCAFileHeader
    val chunks: List<Chunk?>

    init {
        MCAFileReader(file.inputStream()).use { reader ->
            header = reader.readHeader()
            chunks = reader.readChunks(header)
            check(chunks.size == 1024)
        }
    }

    fun getBlockStateAt(blockX: Int, blockY: Int, blockZ: Int): ImmutableCompoundTag {
        val chunkId = getChunkIndexByBlock(blockX, blockZ)
        return chunks[chunkId]?.getBlockStateAt(blockX, blockY, blockZ) ?: throw IllegalStateException("Chunk wasn't initialised")
    }

    fun getChunkByBlockAt(blockX: Int, blockZ: Int): Chunk? {
        return chunks[getChunkIndexByBlock(blockX, blockZ)]
    }

    fun getChunkAt(chunkX: Int, chunkZ: Int): Chunk? {
        return chunks[getChunkIndex(chunkX, chunkZ)]
    }

    fun setBlockStateAt(blockX: Int, blockY: Int, blockZ: Int, block: ImmutableCompoundTag) {
        val chunkId = getChunkIndexByBlock(blockX, blockZ)
        chunks[chunkId]?.setBlockStateAt(blockX, blockY, blockZ, block) ?: throw IllegalStateException("Chunk wasn't initialised")
    }

    fun write(stream: OutputStream) {
        val serialisedChunks = chunks.map { it?.toByteArray() }
        header.updateHeader(serialisedChunks)

        header.write(stream)
        serialisedChunks.asSequence()
            .filterNotNull()
            .forEach(stream::write)
    }
}