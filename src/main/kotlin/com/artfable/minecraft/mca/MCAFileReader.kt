package com.artfable.minecraft.mca

import java.io.Closeable
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.Arrays
import java.util.logging.Logger

/**
 * @author artfable
 * @since 14/06/2022
 */
class MCAFileReader(private val inputStream: InputStream): Closeable {
    private companion object {
        const val SECTOR_SIZE = 4096

        private val log: Logger = Logger.getLogger(MCAFileReader::class.simpleName)
    }

    fun readHeader(): MCAFileHeader {
        val locations = Array(1024) { Pair<UInt, UByte>(0u, 0u) }
        val timestamps = Array(1024) { 0u }
        val locationsByte = inputStream.readNBytes(SECTOR_SIZE)
        val timestampsByte = inputStream.readNBytes(SECTOR_SIZE)

        for (i in 0 until SECTOR_SIZE step 4) {
//            println("$i| ${locationsByte[i].toUInt()} ${locationsByte[i + 1].toUInt()} ${locationsByte[i + 2].toUInt()} ${locationsByte[i + 3].toUInt()}")
            locations[i / 4] =  Pair(concatBytes(i, locationsByte, 3), locationsByte[i + 3].toUByte())
            timestamps[i / 4] =  concatBytes(i, timestampsByte, 4)

//            if (127u <= locations[i / 4].first && locations[i / 4].first < 260u) {
//                println("${locations[i / 4].first} ${locations[i / 4].second}")
//            }
        }

        return MCAFileHeader(locations, timestamps)
    }

    fun readChunks(header: MCAFileHeader): List<Chunk?> {
        var checkIndex = 2u
        val rawChunks = Array(1024) { byteArrayOf() }
        val chunks: MutableList<Chunk?> = ArrayList(1024)

        header.locations.asSequence()
            .mapIndexed { index, location -> Triple(index, location.first, location.second) }
            .filter { it.second != 0u }
            .sortedBy(Triple<Int, UInt, UByte>::second)
            .forEach { (index, offset, size) ->
//                println("$checkIndex ${info.second}")
                if (checkIndex != offset) {
                    log.warning("wrong offset in $index ($offset), expected: $checkIndex")

                    inputStream.skip(SECTOR_SIZE * (offset - checkIndex).toLong())
                    checkIndex = offset
                }
                checkIndex += size

                rawChunks[index] = inputStream.readNBytes(SECTOR_SIZE * size.toInt())

//                if (index == 0) println(rawChunks[0].contentToString())
//                if (index == 0) {
//                    val file = File("./src/test_com_class.txt")
//                    file.createNewFile()
//                    file.writeText(rawChunks[0].joinToString(" "))
//                }
//                if (info.second == 1325u) { // 1323 1325 1328
//                    println("${info.second}| ${info.third}| " + rawChunks[info.first].joinToString(" "))
//                }
            }

        for (rawChunk in rawChunks) {
            chunks.add(if (rawChunk.isEmpty()) null else parseChunk(rawChunk))
        }

        return chunks
    }

    private fun parseChunk(rawChunk: ByteArray): Chunk {
        val chunkSize = concatBytes(0, rawChunk, 4).toInt()
        val compressionType = rawChunk[4].toUByte()
        val rawData = rawChunk.copyOfRange(5, 5 + chunkSize)

        return Chunk(compressionType, rawData)
    }

    /**
     * @param start - offset for bytes to concat from [bytes]
     * @param bytes - source of bytes for concatenation
     * @param amount - how many bytes from [bytes] should be concat together
     */
    private fun concatBytes(start: Int, bytes: ByteArray, amount: Int): UInt {
        var accumulator = bytes[start].toUByte().toUInt()
        for (i in 1 until amount) {
            accumulator = concatByte(accumulator, bytes[start + i])
        }
        return accumulator
    }

    private fun concatByte(accumulator: UInt, byte: Byte): UInt {
        return (accumulator shl 8) or byte.toUByte().toUInt()
    }

    override fun close() {
        inputStream.close()
    }

}