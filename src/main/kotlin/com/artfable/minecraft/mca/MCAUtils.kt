package com.artfable.minecraft.mca

/**
 * @author artfable
 * @since 14/06/2022
 */
fun blockToChunk(block: Int): Int {
    return block shr 4
}

fun blockToRegion(block: Int): Int {
    return block shr 9
}

fun chunkToRegion(chunk: Int): Int {
    return chunk shr 5
}

fun regionToChunk(region: Int): Int {
    return region shl 5
}

fun regionToBlock(region: Int): Int {
    return region shl 9
}

fun chunkToBlock(chunk: Int): Int {
    return chunk shl 4
}

fun getChunkIndex(chunkX: Int, chunkZ: Int): Int {
    return (chunkX and 31) + (chunkZ and 31) * 32
}

fun getChunkIndexByBlock(blockX: Int, blockZ: Int): Int {
    return getChunkIndex(blockToChunk(blockX), blockToChunk(blockZ))
}

fun getBlockIndex(blockX: Int, blockY: Int, blockZ: Int): Int {
    return (blockY and 15) * 16 * 16 + (blockZ and 15) * 16 + (blockX and 15)
}