package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataOutputStream
import java.io.OutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class EndTag private constructor(): AbstractTag(TagId.END, null) {
    companion object {
        val INSTANCE = EndTag()
    }

    override fun writeTag(stream: DataOutputStream) {
        stream.writeByte(tagId.id)
    }

    override fun toString(): String {
        return "EndTag"
    }
}