package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class FloatTag: AbstractTag {
    val float: Float

    constructor(name: String?, float: Float): super(TagId.FLOAT, name) {
        this.float = float
    }

    constructor(dataStream: DataInputStream): super(TagId.FLOAT, dataStream) {
        float = dataStream.readFloat()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.FLOAT, name) {
        float = dataStream.readFloat()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeFloat(float)
    }

    override fun toString(): String {
        return "FloatTag([$name]$float)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as FloatTag

        if (float != other.float) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + float.hashCode()
        return result
    }
}