package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class IntArrayTag: AbstractTag {
    val array: IntArray

    constructor(name: String?, array: IntArray): super(TagId.INT_ARRAY, name) {
        this.array = array
    }

    constructor(dataStream: DataInputStream): super(TagId.INT_ARRAY, dataStream) {
        array = readPayload(dataStream)
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.INT_ARRAY, name) {
        array = readPayload(dataStream)
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeInt(array.size)
        array.forEach { stream.writeInt(it) }
    }

    private fun readPayload(dataStream: DataInputStream): IntArray {
        val size = dataStream.readInt()
        val payload = IntArray(size)

        for (i in 0 until size) {
            payload[i] = dataStream.readInt()
        }

        return payload
    }

    override fun toString(): String {
        return "IntArrayTag([$name]${array.contentToString()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as IntArrayTag

        if (!array.contentEquals(other.array)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + array.contentHashCode()
        return result
    }
}