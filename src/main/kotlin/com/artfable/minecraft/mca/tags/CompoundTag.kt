package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class CompoundTag: AbstractTag, ImmutableCompoundTag {
    override val tags: MutableList<Tag>

    constructor(name: String?, tags: List<Tag>): super(TagId.COMPOUND, name) {
        this.tags = ArrayList(tags)
    }

    constructor(dataStream: DataInputStream): super(TagId.COMPOUND, dataStream) {
        tags = readPayload(dataStream)
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.COMPOUND, name) {
        tags = readPayload(dataStream)
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        tags.forEach { it.writeTag(stream) }
        EndTag.INSTANCE.writeTag(stream)
    }

    private fun readPayload(dataStream: DataInputStream): MutableList<Tag> {
        return sequence {
            var tag = readTag(dataStream)
            while (tag !is EndTag) {
                yield(tag)
                tag = readTag(dataStream)
            }
        }.toMutableList()
    }

    override fun copy(): CompoundTag {
        return CompoundTag(name, tags)
    }

    override fun toString(): String {
        return "CompoundTag([$name]$tags)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as CompoundTag

        if (tags != other.tags) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + tags.hashCode()
        return result
    }
}

interface ImmutableCompoundTag: Tag {

    val tags: List<Tag>

    fun getTagByName(name: String): Tag? {
        return tags.find { it.name ==  name }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Tag> getTagBy(name: String): T? {
        return getTagByName(name) as T?
    }

    fun toCompoundTag(): CompoundTag {
        return CompoundTag(name, tags)
    }

    fun copy(): ImmutableCompoundTag
}