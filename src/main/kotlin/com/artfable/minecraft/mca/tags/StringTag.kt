package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.OutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class StringTag: AbstractTag {
    val text: String

    constructor(name: String?, text: String): super(TagId.STRING, name) {
        this.text = text
    }

    constructor(dataStream: DataInputStream): super(TagId.STRING, dataStream) {
        this.text = dataStream.readUTF()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.STRING, name) {
        this.text = dataStream.readUTF()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeUTF(text)
    }

    override fun toString(): String {
        return "StringTag([$name]'$text')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as StringTag

        if (text != other.text) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + text.hashCode()
        return result
    }
}