package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class IntTag: AbstractTag {
    val int: Int

    constructor(name: String?, int: Int): super(TagId.INT, name) {
        this.int = int
    }

    constructor(dataStream: DataInputStream): super(TagId.INT, dataStream) {
        int = dataStream.readInt()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.INT, name) {
        int = dataStream.readInt()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeInt(int)
    }

    override fun toString(): String {
        return "IntTag([$name]$int)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as IntTag

        if (int != other.int) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + int
        return result
    }
}