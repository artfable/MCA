package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class ByteArrayTag: AbstractTag {
    val byteArray: ByteArray

    constructor(name: String?, byteArray: ByteArray): super(TagId.BYTE_ARRAY, name) {
        this.byteArray = byteArray
    }

    constructor(dataStream: DataInputStream): super(TagId.BYTE_ARRAY, dataStream) {
        byteArray = readPayload(dataStream)
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.BYTE_ARRAY, name) {
        byteArray = readPayload(dataStream)
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeInt(byteArray.size)
        byteArray.forEach { stream.writeByte(it.toUByte().toInt()) }
    }

    private fun readPayload(dataStream: DataInputStream): ByteArray {
        val size = dataStream.readInt()
        val byteArray = ByteArray(size)

        for (i in 0 until size) {
            byteArray[i] = dataStream.readByte()
        }

        return byteArray
    }

    override fun toString(): String {
        return "ByteArrayTag([$name]${byteArray.contentToString()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as ByteArrayTag

        if (!byteArray.contentEquals(other.byteArray)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + byteArray.contentHashCode()
        return result
    }

}