package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class ListTag: AbstractTag {
    val tags: MutableList<Tag>
    val tagsId: TagId

    constructor(name: String?, tags: List<Tag>, tagsId: TagId): super(TagId.LIST, name) {
        this.tags = ArrayList(tags)
        this.tagsId = tagsId
    }

    constructor(dataStream: DataInputStream): super(TagId.LIST, dataStream) {
        tagsId = TagId.getById(dataStream.readByte().toInt())
        tags = readPayload(dataStream)
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.LIST, name) {
        tagsId = TagId.getById(dataStream.readByte().toInt())
        tags = readPayload(dataStream)
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeByte(tagsId.id)
        stream.writeInt(tags.size)
        tags.forEach { tag -> tag.writeTag(stream) }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getTagBy(index: Int): T {
        return tags[index] as T
    }

    private fun readPayload(dataStream: DataInputStream): MutableList<Tag> {
        val size = dataStream.readInt()
        val payload = ArrayList<Tag>(size)

        for (i in 0 until size) {
            payload.add(readNoNameTag(dataStream, tagsId))
        }

        return payload
    }

    override fun toString(): String {
        return "ListTag([$name][$tagsId]$tags)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as ListTag

        if (tags != other.tags) return false
        if (tagsId != other.tagsId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + tags.hashCode()
        result = 31 * result + tagsId.hashCode()
        return result
    }
}