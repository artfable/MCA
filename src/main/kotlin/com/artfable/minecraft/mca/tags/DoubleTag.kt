package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class DoubleTag: AbstractTag {
    val double: Double

    constructor(name: String?, double: Double): super(TagId.DOUBLE, name) {
        this.double = double
    }

    constructor(dataStream: DataInputStream): super(TagId.DOUBLE, dataStream) {
        double = dataStream.readDouble()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.DOUBLE, name) {
        double = dataStream.readDouble()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeDouble(double)
    }

    override fun toString(): String {
        return "DoubleTag([$name]$double)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as DoubleTag

        if (double != other.double) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + double.hashCode()
        return result
    }
}