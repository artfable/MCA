package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class ShortTag: AbstractTag {
    val short: Short

    constructor(name: String?, short: Short): super(TagId.SHORT, name) {
        this.short = short
    }

    constructor(dataStream: DataInputStream): super(TagId.SHORT, dataStream) {
        this.short = dataStream.readShort()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.SHORT, name) {
        this.short = dataStream.readShort()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeShort(short.toUShort().toInt())
    }

    override fun toString(): String {
        return "ShortTag([$name]$short)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as ShortTag

        if (short != other.short) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + short
        return result
    }
}