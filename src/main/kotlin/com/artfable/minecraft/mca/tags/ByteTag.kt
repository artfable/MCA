package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class ByteTag: AbstractTag {

    val byte: Byte

    constructor(name: String?, byte: Byte): super(TagId.BYTE, name) {
        this.byte = byte
    }

    constructor(dataStream: DataInputStream): super(TagId.BYTE, dataStream) {
        byte = dataStream.readByte()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.BYTE, name) {
        byte = dataStream.readByte()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeByte(byte.toUByte().toInt())
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as ByteTag

        if (byte != other.byte) return false

        return true
    }

    override fun hashCode(): Int {
        return byte.toInt()
    }

    override fun toString(): String {
        return "ByteTag([$name]$byte)"
    }
}
