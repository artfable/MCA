package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class LongTag: AbstractTag {
    val long: Long

    constructor(name: String?, long: Long): super(TagId.LONG, name) {
        this.long = long
    }

    constructor(dataStream: DataInputStream): super(TagId.LONG, dataStream) {
        this.long = dataStream.readLong()
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.LONG, name) {
        this.long = dataStream.readLong()
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeLong(long)
    }

    override fun toString(): String {
        return "LongTag([$name]$long)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as LongTag

        if (long != other.long) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + long.hashCode()
        return result
    }
}