package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.InputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
fun readTag(stream: InputStream): AbstractTag {
    val dataStream = DataInputStream(stream)

    return when (val id = dataStream.read()) {
        TagId.END.id -> EndTag.INSTANCE
        TagId.BYTE.id -> ByteTag(dataStream)
        TagId.SHORT.id -> ShortTag(dataStream)
        TagId.INT.id -> IntTag(dataStream)
        TagId.LONG.id -> LongTag(dataStream)
        TagId.FLOAT.id -> FloatTag(dataStream)
        TagId.DOUBLE.id -> DoubleTag(dataStream)
        TagId.BYTE_ARRAY.id -> ByteArrayTag(dataStream)
        TagId.STRING.id -> StringTag(dataStream)
        TagId.LIST.id -> ListTag(dataStream)
        TagId.COMPOUND.id -> CompoundTag(dataStream)
        TagId.INT_ARRAY.id -> IntArrayTag(dataStream)
        TagId.LONG_ARRAY.id -> LongArrayTag(dataStream)
        else -> throw IllegalArgumentException("No tag found [$id]")
    }
}

fun readNoNameTag(dataStream: DataInputStream, tagId: TagId): AbstractTag {
    return when (tagId) {
        TagId.END -> EndTag.INSTANCE
        TagId.BYTE -> ByteTag(null, dataStream)
        TagId.SHORT -> ShortTag(null, dataStream)
        TagId.INT -> IntTag(null, dataStream)
        TagId.LONG -> LongTag(null, dataStream)
        TagId.FLOAT -> FloatTag(null, dataStream)
        TagId.DOUBLE -> DoubleTag(null, dataStream)
        TagId.BYTE_ARRAY -> ByteArrayTag(null, dataStream)
        TagId.STRING -> StringTag(null, dataStream)
        TagId.LIST -> ListTag(null, dataStream)
        TagId.COMPOUND -> CompoundTag(null, dataStream)
        TagId.INT_ARRAY -> IntArrayTag(null, dataStream)
        TagId.LONG_ARRAY -> LongArrayTag(null, dataStream)
    }
}