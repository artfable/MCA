package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
class LongArrayTag: AbstractTag {
    val array: LongArray

    constructor(name: String?, array: LongArray): super(TagId.LONG_ARRAY, name) {
        this.array = array
    }

    constructor(dataStream: DataInputStream): super(TagId.LONG_ARRAY, dataStream) {
        array = readPayload(dataStream)
    }

    constructor(name: String?, dataStream: DataInputStream): super(TagId.LONG_ARRAY, name) {
        array = readPayload(dataStream)
    }

    override fun writeTag(stream: DataOutputStream) {
        super.writeTag(stream)
        stream.writeInt(array.size)
        array.forEach { long ->
            stream.writeLong(long)
        }
    }

    private fun readPayload(dataStream: DataInputStream): LongArray {
        val size = dataStream.readInt()
        val payload = LongArray(size)

        for (i in 0 until size) {
            payload[i] = dataStream.readLong()
        }

        return payload
    }

    override fun toString(): String {
        return "LongArrayTag([$name]${array.contentToString()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as LongArrayTag

        if (!array.contentEquals(other.array)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + array.contentHashCode()
        return result
    }
}