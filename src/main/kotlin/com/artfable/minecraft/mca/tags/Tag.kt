package com.artfable.minecraft.mca.tags

import com.artfable.minecraft.mca.tags.Tag.TagId
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.OutputStream

/**
 * @author artfable
 * @since 19/06/2022
 */
abstract class AbstractTag: Tag {

    final override val name: String?
    val tagId: TagId

    constructor(tagId: TagId, name: String?) {
        this.name = name
        this.tagId = tagId
    }

    constructor(tagId: TagId, dataStream: DataInputStream) {
        name = dataStream.readUTF()
        this.tagId = tagId
    }

    override fun writeTag(stream: DataOutputStream) {
        name?.let {
            stream.writeByte(tagId.id)
            stream.writeUTF(name)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractTag

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}

interface Tag {
    enum class TagId(val id: Int) {
        END(0),
        BYTE(1),
        SHORT(2),
        INT(3),
        LONG(4),
        FLOAT(5),
        DOUBLE(6),
        BYTE_ARRAY(7),
        STRING(8),
        LIST(9),
        COMPOUND(10),
        INT_ARRAY(11),
        LONG_ARRAY(12);

        companion object {
            fun getById(id: Int): TagId {
                return TagId.values().find { it.id == id } ?: throw IllegalArgumentException("No tag for id $id")
            }
        }
    }

    val name: String?

    fun writeTag(stream: DataOutputStream)
}
