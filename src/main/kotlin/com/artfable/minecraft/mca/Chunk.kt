package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.*
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.util.logging.Logger
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterInputStream
import kotlin.math.ceil

/**
 * @author artfable
 * @since 18/06/2022
 */
class Chunk(
    val compressionType: UByte,
    rawData: ByteArray
) {

    private companion object {
        private val log: Logger = Logger.getLogger(Chunk::class.simpleName)
    }

    private val sections: MutableMap<Byte, Section> = mutableMapOf()
    val decompressedData: CompoundTag

    init {
        check(compressionType == 2u.toUByte())

        decompressedData = decompress(rawData)
    }

    private fun decompress(rawData: ByteArray): CompoundTag {
        val tag = InflaterInputStream(rawData.inputStream()).use { readTag(it) }

        return if (tag is CompoundTag) tag else throw IllegalArgumentException("Chunk data should start from CompoundTag, not ${tag::class}")
    }

    fun getBlockStateAt(blockX: Int, blockY: Int, blockZ: Int): ImmutableCompoundTag {
        checkStatus()
        checkCoordinates(blockX, blockZ)
//        val y = listOf(decompressedData.getTagByName("xPos")!!, decompressedData.getTagByName("yPos")!!, decompressedData.getTagByName("zPos")!!)
//
        val section = sections.computeIfAbsent(blockToChunk(blockY).toByte()) {
                sectionY -> getSectionAt(sectionY, decompressedData.getTagByName("sections") as ListTag)
        }
        return section.getBlockStateAt(getBlockIndex(blockX, blockY, blockZ))
    }

    fun setBlockStateAt(blockX: Int, blockY: Int, blockZ: Int, block: ImmutableCompoundTag) {
        checkStatus()
        checkCoordinates(blockX, blockZ)

        val section = sections.computeIfAbsent(blockToChunk(blockY).toByte()) {
                sectionY -> getSectionAt(sectionY, decompressedData.getTagByName("sections") as ListTag)
        }

        section.setBlockStateAt(getBlockIndex(blockX, blockY, blockZ), block)
    }

    fun toByteArray(): ByteArray {
        updateSections()

        val compressedDataOutputStream = ByteArrayOutputStream()
        DataOutputStream(DeflaterOutputStream(compressedDataOutputStream)).use { stream ->
            decompressedData.writeTag(stream)
            stream.flush()
        }

        val compressedData = compressedDataOutputStream.toByteArray()
        val chunkOutputStream = ByteArrayOutputStream()
        DataOutputStream(chunkOutputStream).use { stream ->
            stream.writeInt(compressedData.size)
            stream.writeByte(compressionType.toInt())
            stream.write(compressedData)
            stream.flush()
        }

        val chunkSize = ceil((5 + compressedData.size) / 4096.0) * 4096

        return chunkOutputStream.toByteArray().copyInto(ByteArray(chunkSize.toInt()))
    }

    private fun updateSections() {
        val rawSections = decompressedData.getTagByName("sections") as ListTag

        rawSections.tags.forEach { rawSection ->
            val y = ((rawSection as CompoundTag).getTagByName("Y") as ByteTag).byte
            val blockStates = rawSection.getTagBy<CompoundTag>("block_states") ?: run {
                log.warning("block_states is missing, skipping the section, y: $y")
                return@forEach
            }
            sections[y]?.let { section ->
                val rawData = section.toRawData()
                val paletteIndex = blockStates.tags.indexOfFirst { tag -> tag.name == "palette" }
                val dataIndex = blockStates.tags.indexOfFirst { tag -> tag.name == "data" }
                blockStates.tags[paletteIndex] = rawData.first
                if (dataIndex == -1) { //TODO: check palette
                    blockStates.tags.add(rawData.second)
                } else {
                    blockStates.tags[dataIndex] = rawData.second
                }
            }
        }
    }

    /**
     * The only reason to have binSearch here instead of calculating shift is an assumption that the section can be missing
     */
    private fun getSectionAt(sectionY: Byte, sections: ListTag): Section {
        val ys = sections.tags.mapNotNull { (it as CompoundTag).getTagByName("Y") }.map { it as ByteTag }
        var lo = 0
        var hi = sections.tags.size

        while (lo != hi) {
            val mid = lo + (hi - lo) / 2
            if (ys[mid].byte == sectionY) {
                lo = mid
                hi = mid
            } else if (ys[mid].byte > sectionY) {
                hi = mid
            } else {
                lo = mid
            }
        }

        val rawSection = if (ys[lo].byte == sectionY) sections.tags[lo] as CompoundTag else throw IllegalArgumentException("No section $sectionY in the Chunk")

        val blockStates = rawSection.getTagByName("block_states") as CompoundTag
        val palette = blockStates.getTagByName("palette") as ListTag
        val data = blockStates.getTagByName("data") as LongArrayTag?

        return Section(sectionY, palette, data)
    }

    private fun checkStatus() {
        val status = (decompressedData.getTagByName("Status") as StringTag?)?.text
        if (status != "minecraft:full") {
            throw IllegalStateException("Chunk isn't fully loaded, status: $status")
        }
    }

    private fun checkCoordinates(blockX: Int, blockZ: Int) {
        val chunkX = (decompressedData.getTagByName("xPos") as IntTag).int
        val chunkZ = (decompressedData.getTagByName("zPos") as IntTag).int

        require(blockToChunk(blockX) == chunkX && blockToChunk(blockZ) == chunkZ) {
            "Coordinates ($blockX, $blockZ) are outside of the chunk"
        }
    }
}