package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.CompoundTag
import com.artfable.minecraft.mca.tags.ImmutableCompoundTag
import java.io.File
import java.io.FileOutputStream

/**
 * @author aveselov
 * @since 03/07/2021
 */
class MCADir(path: String) {
    private val dir: File = File(path)
    private val updatedFiles = mutableMapOf<String, MCAFile>()
    private val loadedFiles = mutableMapOf<String, MCAFile>()

    init {
        check(dir.exists()) { "Directory $dir doesn't exist" }
        check(dir.isDirectory) { "$dir not a directory" }
    }

    fun getBlockStateAt(blockX: Int, blockY: Int, blockZ: Int): ImmutableCompoundTag {
        val mcaFile = getMCAFileFromBlock(blockX, blockZ)

        return mcaFile.getBlockStateAt(blockX, blockY, blockZ)
    }

    fun getChunkByBlockCoordinates(blockX: Int, blockZ: Int): Chunk? {
        val mcaFile = getMCAFileFromBlock(blockX, blockZ)

        return mcaFile.getChunkByBlockAt(blockX, blockZ)
    }

    fun setBlockStateAt(blockX: Int, blockY: Int, blockZ: Int, block: ImmutableCompoundTag) {
        val mcaFile = getMCAFileFromBlock(blockX, blockZ)
        mcaFile.setBlockStateAt(blockX, blockY, blockZ, block)
        updatedFiles[getFileNameFromBlock(blockX, blockZ)] = mcaFile
    }
//
//    fun setFigureStateAt(start: Coordinate, figure: Figure, cleanup: Boolean) {
//        setFigureStateAt(start, figure, 0,  cleanup)
//    }
//
//    fun setFigureStateAt(start: Coordinate, figure: Figure, yNumberOfTurns: Int = 0,  cleanup: Boolean = false) {
//        figure.rotateAroundY(yNumberOfTurns)
//
//        figure.points.asSequence()
//            .map { Pair(start + it.key, it.value) }
//            .forEach { (coordinate, block) ->
//                setBlockStateAt(coordinate, block.tag, cleanup)
//            }
//    }

    fun writeUpdated() {
        updatedFiles.forEach {(name, file) ->
            // TODO: temp files in case of exception
            FileOutputStream(dir.resolve(name)).use { stream ->
                file.write(stream)
            }
        }

        updatedFiles.clear()
    }

    fun getMCAFileFromBlock(blockX: Int, blockZ: Int): MCAFile {
        val fileName = getFileNameFromBlock(blockX, blockZ)
        val mcaFile =  loadedFiles[fileName] ?: MCAFile(getFileFromBlock(blockX, blockZ))
        loadedFiles[fileName] = mcaFile

        return mcaFile
    }

    private fun getFileFromBlock(blockX: Int, blockZ: Int): File {
        return dir.resolve(getFileNameFromBlock(blockX, blockZ))
    }

    private fun getFileNameFromBlock(blockX: Int, blockZ: Int): String {
        return "r.${blockToRegion(blockX)}.${blockToRegion(blockZ)}.mca"
    }

//    private fun setBlockStateAt(location: Coordinate, tag: CompoundTag, cleanup: Boolean = false) {
//        val fileName = getFileNameFromBlock(location.x, location.z)
//
//        val mcaFile = updatedFiles[fileName] ?: getMCAFileFromBlock(location.x, location.z)
//        updatedFiles[fileName] = mcaFile
//
//        mcaFile.setBlockStateAt(location.x, location.y, location.z, tag, cleanup)
//    }

}