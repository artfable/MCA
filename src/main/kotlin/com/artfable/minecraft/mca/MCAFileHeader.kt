package com.artfable.minecraft.mca

import java.io.DataOutputStream
import java.io.OutputStream

/**
 * @author artfable
 * @since 14/06/2022
 */
class MCAFileHeader(
    val locations: Array<Pair<UInt, UByte>>,
    val timestamps: Array<UInt>
) {
    init {
        check(locations.size == 1024)
        check(timestamps.size == 1024)
    }

    fun write(stream: OutputStream) {
        locations.forEach { location ->
            stream.write((location.first shr 16).toInt())
            stream.write((location.first shr 8).toInt())
            stream.write(location.first.toInt())
            stream.write(location.second.toInt())
        }
        timestamps.forEach { timestamp ->
            stream.write((timestamp shr 24).toInt())
            stream.write((timestamp shr 16).toInt())
            stream.write((timestamp shr 8).toInt())
            stream.write(timestamp.toInt())
        }
    }

    fun updateHeader(serialisedChunks: List<ByteArray?>) {
        var offset = 2
        serialisedChunks.forEachIndexed { i, chunk ->
            if (chunk != null) {
                val size = chunk.size / 4096
                locations[i] = Pair(offset.toUInt(), size.toUByte())
                offset += size
            } else {
                locations[i] = Pair(0u, 0u)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MCAFileHeader

        if (!locations.contentEquals(other.locations)) return false
        if (!timestamps.contentEquals(other.timestamps)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = locations.contentHashCode()
        result = 31 * result + timestamps.contentHashCode()
        return result
    }
}