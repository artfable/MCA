package com.artfable.minecraft.mca

import com.artfable.minecraft.mca.tags.Tag
import com.artfable.minecraft.mca.tags.readTag
import java.io.DataOutputStream
import java.io.File
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

/**
 * @author artfable
 * 21/02/2024
 */
class DatFile(val file: File) {

    private var data: Tag = GZIPInputStream(file.inputStream()).use { readTag(it) }

    @Suppress("UNCHECKED_CAST")
    fun <T: Tag> getData(): T {
        return data as T
    }

    fun flush() {
        DataOutputStream(GZIPOutputStream(file.outputStream())).use { data.writeTag(it) }
    }
}