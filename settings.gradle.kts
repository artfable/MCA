rootProject.name = "mca"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.namespace?.startsWith("artfable") == true) {
                useModule("${extra["custom.plugins.${requested.id}"]}:${requested.version}")
            }
        }
    }
}
