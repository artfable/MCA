plugins {
    kotlin("jvm") version "1.9.22"
    id("artfable.artifact") version "0.0.4"
    `maven-publish`
}

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

group = "com.artfable.minecraft"
version = "0.2.2"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    testImplementation(kotlin("reflect"))
    testImplementation("org.mockito:mockito-junit-jupiter:4.6.1")
    testImplementation("org.mockito:mockito-inline:4.6.1")
//    testImplementation("io.mockk:mockk:1.12.4")
}

java {
    withSourcesJar()
    withJavadocJar()
    targetCompatibility = JavaVersion.VERSION_17
    sourceCompatibility = JavaVersion.VERSION_17
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    test {
        useJUnitPlatform()
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()

            pom {
                description.set("Lib for parsing .mca files")
                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://gitlab.com/artfable/MCA/-/raw/master/LICENSE")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("artfable")
                        name.set("Artem Veselov")
                        email.set("art-fable@mail.ru")
                    }
                }
            }
        }

        repositories {
            maven {
                url = uri("https://gitlab.com/api/v4/projects/37223094/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = gitlabToken
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }

    }
}
